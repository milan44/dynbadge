package main

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

func GetBadge(label, messsage, color string) ([]byte, error) {
	label = escape(label)
	messsage = escape(messsage)

	resp, err := http.Get("https://img.shields.io/badge/"+label+"-"+messsage+"-"+color)
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func escape(s string) string {
	s = url.QueryEscape(s)
	s = strings.ReplaceAll(s, "+", "%20")
	return s
}