package main

import (
	"flag"
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
	"reflect"
	"strings"
)

var (
	badges = map[string]func(CommandFlags) ([]byte, error){
		"filesize": RenderFilesize,
		"custom": RenderCustom,
	}
	colors = map[string]string {
		"green": "5D9741",
		"blue": "45A4B8",
		"red": "C1282D",
		"orange": "D15D27",
	}
)

type CommandFlags struct {
	OutputFile string
	BadgeType  string
	BadgeValue string
	BadgeColor string
}

func parseFlags() (CommandFlags, error) {
	fl := CommandFlags{
		OutputFile: "./badge.svg",
		BadgeType:  "filesize",
		BadgeValue: "12345",
		BadgeColor: colors["green"],
	}

	keys := reflect.ValueOf(badges).MapKeys()

	out := flag.String("o", "", "Output svg file")
	typ := flag.String("t", "", "Badge type "+fmt.Sprint(keys))
	val := flag.String("v", "", "Badge value")
	col := flag.String("c", "", "Badge color")

	flag.Parse()

	if out != nil && *out != "" {
		err := IsValidFile(*out)
		if err != nil {
			return fl, err
		} else if !strings.HasSuffix(*out, ".svg") {
			return fl, errors.New("output file can only be .svg")
		}

		fl.OutputFile = *out
	}

	if typ != nil && *typ != "" {
		if _, ok := badges[*typ]; !ok {
			return fl, errors.New("invalid badge type")
		}
		fl.BadgeType = *typ
	}

	if val != nil && *val != "" {
		fl.BadgeValue = *val
	}

	if col != nil && *col != "" {
		if _, ok := colors[*col]; !ok {
			return fl, errors.New("invalid badge color")
		}
		fl.BadgeColor = colors[*col]
	}

	return fl, nil
}

func IsValidFile(fp string) error {
	if _, err := os.Stat(fp); err == nil {
		return nil
	}

	var (
		d []byte
	)
	if err := ioutil.WriteFile(fp, d, 0644); err == nil {
		_ = os.Remove(fp)
		return nil
	}

	return errors.New("unable to create file '" + fp + "'")
}
