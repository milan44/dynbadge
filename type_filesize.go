package main

import (
	"errors"
	"fmt"
	"strconv"
)

func RenderFilesize(fl CommandFlags) ([]byte, error) {
	i, err := strconv.Atoi(fl.BadgeValue)
	if err != nil {
		return nil, errors.New("invalid badge value")
	}

	b, err := GetBadge("Size", ByteCountSI(i), fl.BadgeColor)
	if err != nil {
		return nil, errors.New("failed to render")
	}

	return b, nil
}

func ByteCountSI(b int) string {
	const unit = 1000
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f %cB", float64(b)/float64(div), "kMGTPE"[exp])
}