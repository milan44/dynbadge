package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	fl, err := parseFlags()
	if err != nil {
		fmt.Println("Error:", err.Error())
		return
	}

	fmt.Println("Config {\n    Type: " + fl.BadgeType + "\n    Color: #" + fl.BadgeColor + "\n    Value: " + fl.BadgeValue + "\n}")

	res, err := badges[fl.BadgeType](fl)
	if err != nil {
		fmt.Println("Render Error:", err.Error())
		return
	}

	err = ioutil.WriteFile(fl.OutputFile, res, 0777)
	if err != nil {
		fmt.Println("Error:", err.Error())
		return
	}

	fmt.Println("Rendered to " + fl.OutputFile)
}