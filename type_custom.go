package main

import (
	"errors"
	"strings"
)

func RenderCustom(fl CommandFlags) ([]byte, error) {
	lm := strings.Split(fl.BadgeValue, ",")

	if len(lm) != 2 {
		return nil, errors.New("invalid badge value (label,message)")
	}

	b, err := GetBadge(lm[0], lm[1], fl.BadgeColor)
	if err != nil {
		return nil, errors.New("failed to render")
	}

	return b, nil
}